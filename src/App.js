import React from "react";
import classnames from "classnames";
import axios from "axios";

import { Popup } from "./components/Popup";

import "./App.css";

const oneSignalConfig = {
    APP_ID: "a0c42fc5-3426-42c7-8587-dbc34368495f",
    REST_API_KEY: "Nzc3MmVhZDUtZjQxMS00ZDljLWJjMTYtMmU5MDM5NmQ0OTg3",
    url: "https://onesignal.com/api/v1",
};

const sendOnesignalMsg = msg => {
    return axios.post(
        oneSignalConfig.url + "/notifications",
        {
            app_id: oneSignalConfig.APP_ID,
            included_segments: ["All"],
            contents: { en: msg },
        },
        {
            headers: {
                Authorization: "Basic " + oneSignalConfig.REST_API_KEY,
            },
        }
    );
};

const isIos = () => {
    const userAgent = window.navigator.userAgent.toLowerCase();
    return /iphone|ipad|ipod/.test(userAgent);
};

const isInStandaloneMode = () =>
    "standalone" in window.navigator && window.navigator.standalone;

const App = () => {
    const [coords, setCoords] = React.useState({ latitude: 0, longitude: 0 });
    const [showPopup, setShowPopup] = React.useState(
        isIos() && !isInStandaloneMode()
    );

    const [notificationMsg, setNotificationMsg] = React.useState("");

    return (
        <div className={classnames(showPopup && "modal-popup-opened")}>
            <button
                onClick={() => {
                    navigator.geolocation.getCurrentPosition(
                        position => {
                            const { latitude, longitude } = position.coords;
                            setCoords({
                                latitude,
                                longitude,
                            });
                        },
                        err => console.log("err: ", err)
                    );
                }}
            >
                Request geolocation
            </button>
            <div>latitude: {coords.latitude}</div>
            <div>longitude: {coords.longitude}</div>

            <br />
            <br />
            <br />

            <div>Enter notification message</div>
            <div>
                <input
                    onChange={e => setNotificationMsg(e.target.value)}
                    value={notificationMsg}
                />
            </div>
            <div>
                <button
                    onClick={() => {
                        sendOnesignalMsg(notificationMsg)
                            .then(() => setNotificationMsg(""))
                            .catch(error => console.log({ error }));
                    }}
                >
                    Send notification
                </button>
            </div>

            {showPopup && <Popup onClose={() => setShowPopup(false)} />}
        </div>
    );
};

export default App;
